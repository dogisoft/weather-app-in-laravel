<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use DarkSky;
use Illuminate\Http\Request;
use Redirect;

	
class HomepageController extends Controller{

	public function index() {
		return view('homepage');		
	}
	
	public function weather(Request $request){

		if(!$request->municipality || !$request->formated_date){
			return Redirect::to('home')->withFlashMessage('Location or date is missing.');			
		}
		$selected_date = strtotime($request->formated_date);	
		$data = DarkSky::location($request->latitude, $request->longtitude)->atTime($selected_date)->daily();			
		
		return view('weather', ['data' => $data, 'selected_date' => $request->formated_date, 'municipality' => $request->municipality, 'timestamp' => $selected_date]);
	}
	
}
