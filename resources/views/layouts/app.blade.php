<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="csrf-token" content="{{ csrf_token() }}">        
        <title>Weather App</title>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="css/app.css">
		<link rel="stylesheet" href="css/jquery-ui.min.css">	
		<link rel="stylesheet" href="css/teleport-autocomplete.css">		
    </head>
    	
	<body class="bg-light">
		<div class="container">			
		  <div class="py-5 text-center">
			<a href="/"><img class="d-block mx-auto mb-4" src="/images/weather.png" alt="Madewithlove" width="auto" height="auto"></a>
			<h2>How is the weather?</h2>
			<p class="lead">Add your location and the date what you are interested in and submit the form to get detailed weather information. </p>
		  </div>
		  <div class="row">
			 @yield('content')
		  </div>
		  <footer class="my-5 pt-5 text-muted text-center text-small">
			<p class="mb-1">&copy; 2018 Tamas Dogi</p>
		  </footer>		  
		</div>	
	
		<script src="js/app.js"></script>
		<script src="js/teleport-autocomplete.js"></script>	
	</body>
</html>