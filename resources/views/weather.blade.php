@extends('layouts.app')
@section('content')
		
		<div class="col-md-12"><h4>{{$municipality}}, {{$selected_date}}, {{date('l', $timestamp)}}</h4></div>	
		
		<div class="col-md-12">
			<br><br>
			<b>Weather attributes of the selected day:</b>
			<hr><br>
			<div class="list">
				@foreach ($data as $weather)
				<div class="list-item">Summary: {{$weather->summary}}</div>
				<div class="list-item">Icon: {{$weather->icon}}</div>
				<div class="list-item">Sunrise time: {{date('Y-m-d h:i:s', $weather->sunriseTime)}}</div>
				<div class="list-item">Sunset time: {{date('Y-m-d h:i:s', $weather->sunsetTime)}}</div>
				<div class="list-item">Moon phase: {{$weather->moonPhase}}</div>
				<div class="list-item">Precip intensity: {{$weather->precipIntensity}}</div>
				<div class="list-item">Precip intensity max: {{$weather->precipIntensityMax}}</div>
				<div class="list-item">Precip intensity max time: {{date('Y-m-d h:i:s', $weather->precipIntensityMaxTime)}}</div>
				<div class="list-item">Precip probability: {{$weather->precipProbability}}</div>
				<div class="list-item">Precip type: {{( ! empty($weather->precipType))}}</div>
				<div class="list-item">Temperature high: {{$weather->temperatureHigh}}</div>
				<div class="list-item">Temperature high time: {{date('Y-m-d h:i:s', $weather->temperatureHighTime)}}</div>
				<div class="list-item">Temperature low: {{$weather->temperatureLow}}</div>
				<div class="list-item">Temperature low time: {{date('Y-m-d h:i:s', $weather->temperatureLowTime)}}</div>
				<div class="list-item">apparentTemperatureHigh: {{$weather->apparentTemperatureHigh}}</div>
				<div class="list-item">apparentTemperatureHighTime: {{date('Y-m-d h:i:s', $weather->apparentTemperatureHighTime)}}</div>
				<div class="list-item">apparentTemperatureLow: {{$weather->apparentTemperatureLow}}</div>
				<div class="list-item">apparentTemperatureLowTime: {{date('Y-m-d h:i:s', $weather->apparentTemperatureLowTime)}}</div>
				<div class="list-item">dewPoint: {{$weather->dewPoint}}</div>
				<div class="list-item">Humidity: {{$weather->humidity}}</div>
				<div class="list-item">pressure: {{$weather->pressure}}</div>
				<div class="list-item">windSpeed: {{$weather->windSpeed}}</div>
				<div class="list-item">windGust: {{$weather->windGust}}</div>
				<div class="list-item">windGustTime: {{date('Y-m-d h:i:s', $weather->windGustTime)}}</div>
				<div class="list-item">windBearing: {{$weather->windBearing}}</div>
				<div class="list-item">cloudCover: {{$weather->cloudCover}}</div>
				<div class="list-item">uvIndex: {{$weather->uvIndex}}</div>
				<div class="list-item">uvIndexTime: {{date('Y-m-d h:i:s', $weather->uvIndexTime)}}</div>				
				<div class="list-item">ozone: {{$weather->ozone}}</div>
				<div class="list-item">temperatureMin: {{$weather->temperatureMin}}</div>
				<div class="list-item">temperatureMinTime: {{date('Y-m-d h:i:s', $weather->temperatureMinTime)}}</div>
				<div class="list-item">temperatureMax: {{$weather->temperatureMax}}</div>
				<div class="list-item">temperatureMaxTime: {{date('Y-m-d h:i:s', $weather->temperatureMaxTime)}}</div>
				<div class="list-item">apparentTemperatureMin: {{$weather->apparentTemperatureMin}}</div>
				<div class="list-item">apparentTemperatureMinTime: {{date('Y-m-d h:i:s', $weather->apparentTemperatureMinTime)}}</div>
				<div class="list-item">apparentTemperatureMax: {{$weather->apparentTemperatureMax}}</div>
				<div class="list-item">apparentTemperatureMaxTime: {{date('Y-m-d h:i:s', $weather->apparentTemperatureMaxTime)}}</div>
				@endforeach			
			</div>		
		</div>
@endsection	