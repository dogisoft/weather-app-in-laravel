@extends('layouts.app')
@section('content')


		@if (Session::has('flash_message'))
			<div class="alert alert-warning">
			  <strong>Warning!</strong> {{ Session::get('flash_message') }}
			</div>			
		@endif
			
			
        <div class="col-md-12 order-md-1">
          <h4 class="mb-3">Location</h4>
          
		   <form method="POST" action="/weather" class="needs-validation" novalidate>

			<div class="row">
              <div class="col-md-12 mb-3">	
				<div class="results"></div>			  
				<input tabindex="1" autocomplete="off" type="text" class="my-input form-control" id="municipality" name="municipality" placeholder="(Town, City etc.)" />		
				<input type="hidden" name="latitude" id="latitude" value="">
				<input type="hidden" name="longtitude" id="longtitude" value="">              
			  </div>
			</div>
	   
			<div class="row">
              <div class="col-md-12 mb-3">
                <h4 class="mb-3">Date</h4>
				<input name="formated_date" type="text" class="form-control" id="datepicker" placeholder="Click here to start" />
                <div class="invalid-feedback">
                  Valid last name is required.
                </div>
              </div>
			</div>
			
			<button id="submit" class="btn btn-primary btn-lg btn-block" type="submit">Fetch data</button>				
			<input type="hidden" name="_token" value="{{ csrf_token() }}">  
            
          </form>		  		 
        </div>
		
@endsection		