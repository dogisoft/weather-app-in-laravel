
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

$( function() {
	
    $( "#datepicker" ).datepicker({ minDate: -30, maxDate: "+14D" });
	
	var $results = document.querySelector('.results');
	if($results){
		var appendToResult = $results.insertAdjacentHTML.bind($results, 'afterend');
		TeleportAutocomplete.init('.my-input').on('change', function(value) {
			appendToResult('<div><input name=coordinates type="hidden" value=' + JSON.stringify(value.latitude+','+value.longitude, null, 2) + ' id=geo readonly /></div>');
		});	
	}	
});

$("#municipality").on('focusout', function(){

	$.ajax({
		type: "GET",
		url: "https://maps.googleapis.com/maps/api/geocode/json?language=en&address="+$("#municipality").val(),
		success: function(data){
				if(data.status == 'OK'){		
					$("#latitude").val(data.results[0].geometry.location.lat);
					$("#longtitude").val(data.results[0].geometry.location.lng);				
			}
		}
    });
});


